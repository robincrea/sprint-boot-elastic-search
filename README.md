# Des planètes, des superheros et des bars

# Marche à suivre

## Récupérer les requète de test sur postman

- Ouvrir Postman
- Cliquer sur "import" -> "import from url"
- Coller l'url `https://www.getpostman.com/collections/0adc3bca904d175d7cb7`
- Cliquer sur Import

## Démarrer serveur ElasticSearch 6.2.4

### Télécharger ElasticSearch

https://www.elastic.co/fr/downloads/past-releases/elasticsearch-6-2-4

### Démmarer le serveur

```bash
# Dans le dossier elastic search
cd elasticsearch-6-2-4
# Run le serveur
./bin/elasticsearch.bat # Pour pc
./bin/elasticsearch # Pour mac
```

## Démarrer le projet springboot

A la racine du projet executer la commande suivante:

```bash
./gradlew bootRun
```

Spring boot créer les index nécessaire au projet dans elastic search

## Ajout des données dans Elastic Search

- Ouvrir postman
- Ouvrir les requêtes `create bars` et `create heroes`
- Adapter les chemin vers les fichier json (Situé à la racine de ce projet: bars.json et heroes.json)
- Exécuter les requètes `create bars` et `create heroes`

_Note: Vous pouvez également exécuter ces requête avec curl_

```bash
# Ajout des superheroes à ElasticSearch
curl -XPUT localhost:9200/_bulk -H "Content-Type: application/json" --data-binary @heroes.json
# Ajout des bars à ElasticSearch
curl -XPUT localhost:9200/_bulk -H "Content-Type: application/json" --data-binary @bars.json
```

## Tester les différentes routes Via Postman

Grace au lien postman vous pouvez tester les différentes requête à l'API.
