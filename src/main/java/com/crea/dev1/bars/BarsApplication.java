package com.crea.dev1.bars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarsApplication.class, args);
	}

}
