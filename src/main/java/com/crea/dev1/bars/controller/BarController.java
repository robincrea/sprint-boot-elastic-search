package com.crea.dev1.bars.controller;

import com.crea.dev1.bars.dao.BarDao;
import com.crea.dev1.bars.model.Bar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(description = "Api pour les optération CRUD sur les bar")
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
@RestController("/bars")
public class BarController {
    @Autowired
    private BarDao barDao;

    @GetMapping("/bars")
    public Page<Bar> getAllBars() {
        Page<Bar> bars = barDao.findAll();
        return bars;
    }

    @GetMapping("/bars/{name}")
    public Bar getByName(@PathVariable String name) {
        Bar bar = barDao.findByName(name);
        return bar;
    }

}