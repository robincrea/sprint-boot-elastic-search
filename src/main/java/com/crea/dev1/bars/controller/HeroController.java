package com.crea.dev1.bars.controller;

import java.util.List;

import com.crea.dev1.bars.dao.HeroDao;
import com.crea.dev1.bars.model.Hero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(description = "Api pour les optération CRUD sur les superheroes")
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
@RestController("/superheroes")
public class HeroController {
    @Autowired
    private HeroDao heroesDao;

    @GetMapping("/superheroes")
    public Page<Hero> getAllHeroes() {
        Page<Hero> heroes = heroesDao.findAll();
        return heroes;
    }

    @GetMapping("/superhero/id/{id}")
    public Hero getById(@PathVariable Long id) {
        Hero hero = heroesDao.findById(id);
        return hero;
    }

    @GetMapping("/superheroes/{name}")
    public List<Hero> findByName(@PathVariable String name) {
        return heroesDao.findByName(name);
    }

}