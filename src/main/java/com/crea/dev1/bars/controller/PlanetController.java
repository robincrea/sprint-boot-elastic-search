package com.crea.dev1.bars.controller;

import java.util.List;

import com.crea.dev1.bars.dao.PlanetDao;
import com.crea.dev1.bars.exception.PlanetNotFoundException;
import com.crea.dev1.bars.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "Api pour les optération CRUD sur les planetes")
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
@RestController("/planets")
public class PlanetController {
    @Autowired
    private PlanetDao planetDao;

    public RestTemplate rest() {
        return new RestTemplate();
    }

    @GetMapping("/planets")
    public List<Planet> getAllPlanets() {
        List<Planet> myPlanets = planetDao.findAll();
        return myPlanets;
    }

    @ApiOperation(value = "Récupère un planete avec un ID donné")
    @RequestMapping(value = "/planets/{id}", method = RequestMethod.GET)
    public Planet getPlanet(@PathVariable int id) {
        Planet foundPlanet = planetDao.findById(id);

        if (foundPlanet == null)
            throw new PlanetNotFoundException("In n'y pas de planete avec l'id: " + id);
        return foundPlanet;
    }

    @GetMapping("/planets/distance-more/{distance}")
    public List<Planet> distanceGreater(@PathVariable Long distance) {
        return planetDao.findByDistanceGreaterThan(distance);
    }

    @GetMapping("/planets/distance-mores/{distance}")
    public List<Planet> distanceHighest(@PathVariable Long price) {
        return planetDao.seekForHighestDistance(price);
    }

    @PostMapping(path = "/planets/add")
    public List<Planet> addPlanet(@RequestBody Planet planet) {
        // code
        planetDao.save(planet);
        return planetDao.findAll();
    }

    @PutMapping(path = "/planets/update")
    public Planet updatePlanet(@RequestBody Planet planet) {
        planetDao.save(planet);
        return planet;
    }

    @DeleteMapping(path = "/planets/delete/{id}")
    public List<Planet> deletePlanet(@PathVariable int id) {
        planetDao.deleteById(id);
        return planetDao.findAll();
    }

}