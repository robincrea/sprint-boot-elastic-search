package com.crea.dev1.bars.dao;

import com.crea.dev1.bars.model.Bar;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarDao extends ElasticsearchRepository<Bar, Integer> {
    public Page<Bar> findAll();

    public Bar findByName(String name);
}