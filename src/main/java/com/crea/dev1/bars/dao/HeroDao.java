package com.crea.dev1.bars.dao;

import java.util.List;

import com.crea.dev1.bars.model.Hero;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeroDao extends ElasticsearchRepository<Hero, Integer> {

    public Page<Hero> findAll();

    public Hero findById(Long id);

    List<Hero> findByName(String title);

}