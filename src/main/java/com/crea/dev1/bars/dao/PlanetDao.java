package com.crea.dev1.bars.dao;

import java.util.List;

import com.crea.dev1.bars.model.Planet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetDao extends JpaRepository<Planet, Integer> {

    public List<Planet> findAll();

    public Planet findById(int id);

    public Planet findByNameLike(String name);

    public List<Planet> findByDistanceGreaterThan(Long distance);

    @Query(value = "select id,name,distance from Planet where distance> :distance", nativeQuery = true)
    List<Planet> seekForHighestDistance(@Param("distance") Long distance);

}