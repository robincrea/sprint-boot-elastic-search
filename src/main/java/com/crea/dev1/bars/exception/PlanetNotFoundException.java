package com.crea.dev1.bars.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PlanetNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PlanetNotFoundException(String s) {
        super(s);
    }
}