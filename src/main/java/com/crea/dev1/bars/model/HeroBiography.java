
package com.crea.dev1.bars.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class HeroBiography {
    private List<String> aliases;
    private String publisher;
    private String alignement;
}