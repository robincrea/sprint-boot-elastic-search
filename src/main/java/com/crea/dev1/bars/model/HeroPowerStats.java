
package com.crea.dev1.bars.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class HeroPowerStats {
    private Long intelligence;
    private Long strength;
    private Long speed;
    private Long durability;
    private Long power;
    private Long combat;
}